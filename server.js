const express = require("express");
const cors = require("cors");
const { dbConnection } = require("./src/config/database");
const router = require("./src/routes/routes");
const errorMiddleware = require("./src/middlewares/errors");
const path = require("path");

app = express();
port = process.env.PORT || 5000;

//db connection
dbConnection();

// Middlewares
// CORS
app.use(cors());
app.use(express.json());
// Routes

app.use("/public", express.static(`${__dirname}/storage`));
app.use("/api", router);
app.use(errorMiddleware);

//listen
app.listen(port, () => {
  console.log("Server On , port : ", port);
});
