const moongose = require("mongoose");
require("dotenv").config();
movieSchema = require("../models/movie.model");

//connection to the DB
const dbConnection = async () => {
  try {
    await moongose.connect(
      process.env.MONGODB_CONNECT ||
        "mongodb+srv://user:1234@cluster0.sdap8.mongodb.net/login",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      }
    );
    console.log("successful connection");
  } catch {
    console.log(
      "error de conexión - dirección de base de datos inexistente/inaccesible"
    );
  }
};

module.exports = {
  dbConnection,
};
