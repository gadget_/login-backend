const User = require("../../models/user.model");
const bcrypt = require("bcryptjs");
module.exports = {
  async getUsers() {
    const user = await User.find();
    return user;
  },
  async getUserById(id) {
    const user =await User.findById(id);
    return user;
  },

  postUsers(name, email, password) {
    return new Promise(async (resolve, reject) => {
      const user = new User({ name, email, password });
      //verificar que no exista el correo
      const verifyMail = await User.findOne({ email });
      if (verifyMail) {
        reject("este correo ya esta registrado");
      } else {
        //encriptar la contraseña
        const salt = bcrypt.genSaltSync();
        user.password = bcrypt.hashSync(password, salt);

        //guardar en bd
        await user.save();
        resolve();
      }
    });
  },
  putUsers(id, password, email, rest) {
    return new Promise(async (resolve, reject) => {
      //validar que el usuario exista
      const userExist = await User.findById(id);
      if (!userExist) {
        reject(`el id ${id} no existe en la base de datos`);
      }

      if (password) {
        const salt = bcrypt.genSaltSync();
        rest.password = bcrypt.hashSync(password, salt);
      }
      //actualizar en la db
      const userDB = await User.findByIdAndUpdate(id, rest);
      resolve();
    });
  },
};
