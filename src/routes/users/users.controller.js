const { getUsers, postUsers, putUsers, getUserById } = require("./ops");

const usersGet = async (req, res) => {
  const result = await getUsers();
  res.json({
    result,
  });
};

const usersById = async (req, res) => {
  const { id } = req.params;
  const result = await getUserById(id);
  res.json({
    result,
  });
};

const usersPost = async (req, res) => {
  //tomar parametros del cuerpo de una petición
  const { name, email, password } = req.body;
  try {
    result = await postUsers(name, email, password);
    res.json({
      msg: "usuario registrado",
      result,
    });
  } catch (error) {
    return res.status(400).json({
      error: "true",
      msg: error,
    });
  }
};

const usersPut = async (req, res) => {
  //tomar parametros de una petición
  const id = req.params.id;
  const { _id, password, email, ...rest } = req.body;
  //Validar que el usuario exista
  try {
    result = await putUsers(id, password, email, rest);
    //edicion exitosa
    res.status(201).json({
      msg: "usuario editado",
    });
  } catch (error) {
    return res.status(400).json({
      error: "true",
      msg: error,
    });
  }
};
module.exports = {
  usersGet,
  usersPost,
  usersPut,
  usersById,
};
