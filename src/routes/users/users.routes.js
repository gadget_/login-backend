const { Router } = require("express");
const { check } = require("express-validator");
const validations = require("../../middlewares/validations");
const { usersGet, usersPut, usersPost , usersById} = require("../users/users.controller");

//const { validateUser } = require("../helpers/dbValidators");
const router = Router();

router.get("/", usersGet);
router.get("/:id",usersById)

router.put(
  "/:id",
  [check("id", "no es un id valido").isMongoId(), validations],
  usersPut
);

router.post(
  "/",
  [
    check("email", "el correo no es valido").isEmail(),
    check("name", "el nombre es obligatorio").not().isEmpty(),
    check(
      "password",
      "la contraseña es obligatoria y debe tener mas de 6 caracteres"
    ).isLength({ min: 6 }),
    validations,
  ],
  usersPost
);

module.exports = router;
