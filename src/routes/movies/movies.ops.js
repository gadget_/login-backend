const Movie = require("../../models/movie.model");
module.exports = {
  async getMovies() {
    const movie = await Movie.find();
    return movie;
  },
  async getMovieById(id) {
    const movie = await Movie.findById(id);
    return movie;
  },

  postMovie(
    nombre,
    director,
    actores,
    canciones,
    comentarios,
    sinopsis,
    anyo,
    posterUrl,
    trailerUrl,
    calificacion
  ) {
    return new Promise(async (resolve, reject) => {
      const movie = new Movie({
        nombre,
        director,
        actores,
        canciones,
        comentarios,
        sinopsis,
        anyo,
        posterUrl,
        trailerUrl,
        calificacion,
      });
      //guardar en bd
      await movie.save();
      resolve();
    });
  },

  addComment(id, comentario) {
    return new Promise(async (resolve, reject) => {
      //validar que la pelicula exista
      const movieExist = await Movie.findById(id);
      if (!movieExist) {
        reject(`el id ${id} no existe en la base de datos`);
      }
      //actualizar en la db
      const movieDB = await Movie.findByIdAndUpdate(id, {
        $push: { comentarios: comentario },
      });
      resolve();
    });
  },
  addCalificacion(id, calificacion) {
    return new Promise(async (resolve, reject) => {
      //validar que la pelicula exista
      const movieExist = await Movie.findById(id);
      if (!movieExist) {
        reject(`el id ${id} no existe en la base de datos`);
      }
      //actualizar en la db
      const movieDB = await Movie.findByIdAndUpdate(id, {
        $push: { calificacion: calificacion },
      });
      resolve();
    });
  },
};
