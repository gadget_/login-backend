const {
  getMovies,
  postMovie,
  getMovieById,
  addComment,
  addCalificacion,
} = require("./movies.ops");

const moviesGet = async (req, res) => {
  const result = await getMovies();
  res.json({
    result,
  });
};

const movieById = async (req, res) => {
  const { id } = req.params;
  const result = await getMovieById(id);
  res.json({
    result,
  });
};

const moviesPost = async (req, res) => {
  //tomar parametros del cuerpo de una petición
  const {
    nombre,
    director,
    actores,
    canciones,
    comentarios,
    sinopsis,
    anyo,
    trailerUrl,
    calificacion,
  } = req.body;

  const posterUrl = `https://moviesapp-and.herokuapp.com/public/${req.file.filename}`;
  try {
    result = await postMovie(
      nombre,
      director,
      actores,
      canciones,
      comentarios,
      sinopsis,
      anyo,
      posterUrl,
      trailerUrl,
      calificacion
    );
    console.log(posterUrl);

    res.json({
      msg: "pelicula registrada",
      result,
    });
  } catch (error) {
    return res.status(400).json({
      error: "true",
      msg: error,
    });
  }
};

const putComment = async (req, res) => {
  //tomar parametros de una petición
  const id = req.params.id;
  const { comentarios } = req.body;
  //Validar que la pelicula exista
  try {
    result = await addComment(id, comentarios);
    //edicion exitosa
    res.status(201).json({
      msg: "comentario añadido",
    });
  } catch (error) {
    return res.status(400).json({
      error: "true",
      msg: error,
    });
  }
};

const putCalificacion = async (req, res) => {
  //tomar parametros de una petición
  const id = req.params.id;
  const { calificacion } = req.body;
  //Validar que la pelicula exista
  try {
    result = await addCalificacion(id, calificacion);
    //edicion exitosa
    res.status(201).json({
      msg: "calificacion añadido",
    });
  } catch (error) {
    return res.status(400).json({
      error: "true",
      msg: error,
    });
  }
};
module.exports = {
  moviesGet,
  movieById,
  moviesPost,
  putComment,
  putCalificacion,
};
