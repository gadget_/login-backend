const { Router } = require("express");
const upload = require("../../middlewares/storage");
const {
  moviesGet,
  movieById,
  moviesPost,
  putComment,
  putCalificacion,
} = require("../movies/movies.controller");

//const { validateUser } = require("../helpers/dbValidators");
const router = Router();

router.get("/", moviesGet);
router.get("/:id", movieById);
router.post("/", upload.single("image"), moviesPost);
router.put("/:id", putComment);
router.put("/calificacion/:id", putCalificacion);

module.exports = router;
