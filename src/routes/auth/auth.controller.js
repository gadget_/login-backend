const User = require("../../models/user.model");
const { validateUser, validatePassword } = require("./ops");
const { generateJWT } = require("../../helpers/generateJWT");

const login = async (req, res) => {
  const { email, password } = req.body;
  try {
    //verificar si el email existe
    const user = await User.findOne({ email });
    //validar usuario
      result = await validateUser(user);
    //validar contraseña
      result = await validatePassword(password, user.password);
    //generar JWT
    const token = await generateJWT(user.id);
    //inicio de sesión exitoso

    res.status(200).json({
      error: "false",
      msg: "inicio de sesión exitoso",
      user,
      token,
    });
  } catch (error) {
    console.log(error);
    return res.status(error.status).json({
      msg:error,
    });
  }
};

module.exports = login;
