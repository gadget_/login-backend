const bcrypt = require("bcryptjs");
module.exports = {
  validatePassword(userPassword, dbPassword) {
    return new Promise((resolve, reject) => {
      const validatePassword = bcrypt.compareSync(userPassword, dbPassword);
      if (!validatePassword) {
        reject("usuario/contraseña inválidos");
      } else {
        resolve();
      }
    });
  },
  validateUser(user) {
    return new Promise((resolve, reject) => {
      if (!user) {
        reject("usuario/contraseña inválidos");
      } else {
        resolve();
      }
    });
  },
};
