const router = require("express").Router();
const auth = require("./auth/auth.routes");
const user = require("./users/users.routes");
const movie = require("./movies/movies.routes");

router.use("/auth", auth);
router.use("/users", user);
router.use("/movies", movie);

module.exports = router;
