const jwt = require("jsonwebtoken");
module.exports = {
  generateJWT(uid) {
    return new Promise((resolve, reject) => {
      const payload = { uid };

      jwt.sign(
        payload,
        process.env.SECRET_KEY || "anderson",
        {
          expiresIn: "5h",
        },
        (err, token) => {
          if (err) {
            reject("no se pudo generar el token");
          } else {
            resolve(token);
          }
        }
      );
    });
  },
};
