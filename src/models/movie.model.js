const { Schema, model } = require("mongoose");

const movieSchema = Schema({
  nombre: String,
  director: String,
  actores: [String],
  canciones: [String],
  comentarios: [String],
  sinopsis: String,
  anyo: Number,
  posterUrl: String,
  trailerUrl: String,
  calificacion: [Number],
});

module.exports = model("Movie", movieSchema);
